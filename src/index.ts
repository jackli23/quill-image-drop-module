import { ImageDrop } from './ImageDrop';
import { Quill } from 'react-quill';

export interface IHandlerImageDropAndPasteOnEditor {
  requestContext;
  url_prefix: string;
  urlConverter: (params: unknown, optionMap?: Map<string, unknown>) => string;
  uploadImagesDroppedAndPasted: (fileList: File[], alias) => Promise<any>;
}

export default class ImageDropUpload extends ImageDrop {
  static imageDropHandler: IHandlerImageDropAndPasteOnEditor;

  constructor(quill: Quill, options = {}) {
    super(quill, options);
  }

  /**
   * Overridden method: Handler for paste event to read pasted files from evt.clipboardData
   * @param {Event} evt
   */
  handlePaste(evt) {
    evt.preventDefault(); //extra line prevent default: save to database and generate a url
    console.log("in paste", evt.clipboardData);
    if (
      evt.clipboardData &&
      evt.clipboardData.items &&
      evt.clipboardData.items.length
    ) {
      console.log("call readfiles");
      this.readFiles(evt.clipboardData.items, this.insert.bind(this));
    }
  }

  /**
   * Overridden method: Extract image URIs a list of files from evt.dataTransfer or evt.clipboardData
   * @param {File[]} files  One or more File objects
   * @param {Function} callback  A function to send each data URI to
   */
  readFiles(files, callback) {
    console.log("start readfiles");
    // check each file for an image
    [].forEach.call(files, (file) => {
      if (
        !file.type.match(
          /^image\/(gif|jpe?g|a?png|svg|webp|bmp|vnd\.microsoft\.icon)/i
        )
      ) {
        // file is not an image
        // Note that some file formats such as psd start with image/* but are not readable
        return;
      }
      // set up file reader
      // const reader = new FileReader();
      // reader.onload = (evt) => {
      // 	callback(evt.target.result);
      // };
      // read the clipboard item or file
      const blob: File = file.getAsFile ? file.getAsFile() : file;
      console.log("in read files", blob);
      if (blob instanceof Blob) {
        //upload files to database
        const handler = ImageDropUpload.imageDropHandler;

        handler
          .uploadImagesDroppedAndPasted([blob], {
            [blob.name]: `${Date.now()}_${blob.name}`,
          })
          .then((res) => {
            //insert into editor
            console.log(res);
            const _id = res[0].data.detail[0].fileId;
            callback(
              `${handler.url_prefix}?${handler.urlConverter({
                id: _id,
                site_db_name: handler.requestContext.settings.site_db_name,
              })}`
            );
          })
          .catch((err) => {
            console.log(err);
          });
        //reader.readAsDataURL(blob);
      }
    });
  }

  static init(uponDropHandler: any) {
    ImageDropUpload.imageDropHandler = uponDropHandler;
    return ImageDropUpload;
  }
}
